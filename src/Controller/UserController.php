<?php
/**
 * Created by IntelliJ IDEA.
 * User: Audran
 * Date: 14/03/2019
 * Time: 17:32
 */

namespace App\Controller;


use App\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    public function createUser(Request $request, ValidatorInterface $validator) {
        $values = explode(",",$request->getContent());

        if (count($values) != 4) {
            return new Response("Erreur : nombre d'arguments incorrects.");
        }

        $user = new Utilisateur();
        $user->setNom($values[0])
            ->setPrenom($values[1])
            ->setEmail($values[2])
            ->setPassword($values[3]);

        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            $reponse = "";
            foreach($errors as $error) {
                $reponse.=$error->getMessage()."\n";
            }
            $reponse.="Veuillez reessayer.";
            return new Response($reponse);
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return new Response("Insertion reussie. Utilisateur crée :
             \n nom : ".$user->getNom().
            "\n prenom : ".$user->getPrenom().
            "\n email : ".$user->getEmail().
            "\n mot de passe: ".$user->getPassword());
    }
}