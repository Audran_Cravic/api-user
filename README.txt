Utiliser l'API :
    - Configurer les paramètres de connexion à la base de données dans le fichier .env
    - Mettre à jour composer avec composer update si nécéssaire
    - Créer la base de données avec la commande php app/console doctrine:database:create
    - Créer la table avec la commande php app/console doctrine:schema:update --force
    - lancer le serveur avec php bin/console server:run
    - lancer la commande curl http://127.0.0.1:8000/newUser -d suivi des valeurs à utiliser pour le nouvel utilisateur entre double côtes et séparées par des ,
      Exemple : curl http://127.0.0.1:8000/newUser -d "audran,cravic,audran.cravic@gmail.com,bonjour"